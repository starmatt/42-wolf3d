/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 15:16:30 by mborde            #+#    #+#             */
/*   Updated: 2014/11/11 00:12:05 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void *ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	size_t		i;
	char		*t1;
	const char	*t2;

	i = 0;
	t1 = dst;
	t2 = src;
	while (i < n && (i == 0 || t2[i - 1] != c))
	{
		t1[i] = t2[i];
		i++;
	}
	if (i > 0 && t2[i - 1] == c)
		return (t1 + i);
	else
		return (NULL);
}
