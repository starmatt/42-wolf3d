/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   texture.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/03 01:55:27 by mborde            #+#    #+#             */
/*   Updated: 2015/06/12 14:54:44 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

Uint32		getpixel(int x, int y, t_wolf *w)
{
	Uint32	*pixels;

	pixels = NULL;
	if (w->r.side == 0 && w->map[w->r.mapx][w->r.mapy] == 2)
		pixels = (Uint32 *)w->i.wall->pixels;
	if (w->r.side == 1 && w->map[w->r.mapx][w->r.mapy] == 2)
		pixels = (Uint32 *)w->i.wall2->pixels;
	if (w->map[w->r.mapx][w->r.mapy] == 3)
		pixels = (Uint32 *)w->i.door->pixels;
	if (w->map[w->r.mapx][w->r.mapy] > 3)
		return (0xFF00000);
	return (pixels[(TEX_H * y) + x]);
}

void		make_color(Uint32 color, Uint32 *rgb, int k, t_wolf *w)
{
	if (w->r.side == 0 && w->r.ray_dx < 0 && k == 0)
		color = (color >> 1) & 8355711;
	else if (w->r.side == 1 && w->r.ray_dy > 0 && k == 0)
		color = (color >> 1) & 8355711;
	rgb[0] = (color & 0x00ff0000) >> 16;
	rgb[1] = (color & 0x0000ff00) >> 8;
	rgb[2] = (color & 0x000000ff);
}

static int	texture_init2(SDL_Surface *tmp, t_wolf *w)
{
	tmp = NULL;
	if ((tmp = SDL_LoadBMP("data/wall2.bmp")) == NULL)
		return (put_error("Error: a texture failed to load."));
	if (tmp->h != TEX_H || tmp->w != TEX_W)
	{
		SDL_FreeSurface(tmp);
		return (put_error("Error: wrong texture format."));
	}
	w->i.wall2 = SDL_ConvertSurfaceFormat(tmp, SDL_PIXELFORMAT_ARGB8888, 0);
	SDL_FreeSurface(tmp);
	return (0);
}

int			texture_init(t_wolf *w)
{
	SDL_Surface	*tmp;

	tmp = NULL;
	if ((tmp = SDL_LoadBMP("data/wall.bmp")) == NULL)
		return (put_error("Error: a texture failed to load."));
	if (tmp->h != TEX_H || tmp->w != TEX_W)
	{
		SDL_FreeSurface(tmp);
		return (put_error("Error: wrong texture format."));
	}
	w->i.wall = SDL_ConvertSurfaceFormat(tmp, SDL_PIXELFORMAT_ARGB8888, 0);
	SDL_FreeSurface(tmp);
	tmp = NULL;
	if ((tmp = SDL_LoadBMP("data/door.bmp")) == NULL)
		return (put_error("Error: a texture failed to load."));
	if (tmp->h != TEX_H || tmp->w != TEX_W)
	{
		SDL_FreeSurface(tmp);
		return (put_error("Error: wrong texture format."));
	}
	w->i.door = SDL_ConvertSurfaceFormat(tmp, SDL_PIXELFORMAT_ARGB8888, 0);
	SDL_FreeSurface(tmp);
	if (texture_init2(tmp, w) == -1)
		return (-1);
	return (0);
}
