/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/27 18:05:42 by mborde            #+#    #+#             */
/*   Updated: 2015/06/11 17:39:20 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void		rotate(t_wolf *w)
{
	double	p_dirx;
	double	p_planx;

	if (w->m.r_right)
	{
		p_dirx = w->c.dirx;
		w->c.dirx = w->c.dirx * cos(-w->m.rota) - w->c.diry * sin(-w->m.rota);
		w->c.diry = p_dirx * sin(-w->m.rota) + w->c.diry * cos(-w->m.rota);
		p_planx = w->c.planx;
		w->c.planx =
			w->c.planx * cos(-w->m.rota) - w->c.plany * sin(-w->m.rota);
		w->c.plany = p_planx * sin(-w->m.rota) + w->c.plany * cos(-w->m.rota);
	}
	if (w->m.r_left)
	{
		p_dirx = w->c.dirx;
		w->c.dirx = w->c.dirx * cos(w->m.rota) - w->c.diry * sin(w->m.rota);
		w->c.diry = p_dirx * sin(w->m.rota) + w->c.diry * cos(w->m.rota);
		p_planx = w->c.planx;
		w->c.planx = w->c.planx * cos(w->m.rota) - w->c.plany * sin(w->m.rota);
		w->c.plany = p_planx * sin(w->m.rota) + w->c.plany * cos(w->m.rota);
	}
}

static void	move_3(t_wolf *w)
{
	if (w->m.up && w->m.look < 400)
		w->m.look += 60;
	if (w->m.down && w->m.look > -400)
		w->m.look -= 60;
	if (w->m.run)
		w->m.movs = 0.1 * 4;
	else
		w->m.movs = 0.1;
}

static void	move_2(t_wolf *w)
{
	if (w->m.back)
	{
		if (w->map[(int)(w->c.posx - w->c.dirx * w->m.movs)][(int)w->c.posy]
				> 1)
			w->c.posx += w->c.dirx * w->m.movs;
		if (w->map[(int)w->c.posx][(int)(w->c.posy - w->c.diry * w->m.movs)]
				> 1)
			w->c.posy += w->c.diry * w->m.movs;
		w->c.posx -= w->c.dirx * w->m.movs;
		w->c.posy -= w->c.diry * w->m.movs;
	}
	if (w->m.right)
	{
		if (w->map[(int)(w->c.posx + w->c.diry * w->m.movs)][(int)w->c.posy]
				> 1)
			w->c.posx -= w->c.diry * w->m.movs;
		if (w->map[(int)w->c.posx][(int)(w->c.posy + -w->c.dirx * w->m.movs)]
				> 1)
			w->c.posy -= -w->c.dirx * w->m.movs;
		w->c.posx += w->c.diry * w->m.movs;
		w->c.posy += -w->c.dirx * w->m.movs;
	}
}

void		move(t_wolf *w)
{
	if (w->m.forw)
	{
		if (w->map[(int)(w->c.posx + w->c.dirx * w->m.movs)][(int)w->c.posy]
				> 1)
			w->c.posx -= w->c.dirx * w->m.movs;
		if (w->map[(int)w->c.posx][(int)(w->c.posy + w->c.diry * w->m.movs)]
				> 1)
			w->c.posy -= w->c.diry * w->m.movs;
		w->c.posx += w->c.dirx * w->m.movs;
		w->c.posy += w->c.diry * w->m.movs;
	}
	if (w->m.left)
	{
		if (w->map[(int)(w->c.posx + -w->c.diry * w->m.movs)][(int)w->c.posy]
				> 1)
			w->c.posx -= -w->c.diry * w->m.movs;
		if (w->map[(int)w->c.posx][(int)(w->c.posy + w->c.dirx * w->m.movs)]
				> 1)
			w->c.posy -= w->c.dirx * w->m.movs;
		w->c.posx += -w->c.diry * w->m.movs;
		w->c.posy += w->c.dirx * w->m.movs;
	}
	move_2(w);
	move_3(w);
}
