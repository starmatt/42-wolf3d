/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/29 10:14:00 by mborde            #+#    #+#             */
/*   Updated: 2015/06/12 20:02:19 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static int	init_sdl(t_wolf *w)
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
		return (put_error("Error: SDL failed to initialize."));
	if ((w->win = SDL_CreateWindow("Wolf3D", SDL_WINDOWPOS_CENTERED,
					SDL_WINDOWPOS_CENTERED, WIN_X, WIN_Y, SDL_WINDOW_SHOWN))
			== NULL)
		return (put_error("Error: window could not be created."));
	if ((w->ren = SDL_CreateRenderer(w->win, -1, SDL_RENDERER_ACCELERATED))
			== NULL)
		return (put_error("Error: renderer could not be created."));
	sound_init(w);
	text_init(w);
	return (0);
}

void		init_c(t_wolf *w)
{
	w->c.posx = 22.5;
	w->c.posy = 2.5;
	w->c.dirx = -1;
	w->c.diry = 0;
	w->c.planx = 0;
	w->c.plany = 0.64;
	w->m.movs = 0.1;
	w->m.rota = 0.1 * 2;
	w->m.right = 0;
	w->m.left = 0;
	w->m.forw = 0;
	w->m.back = 0;
	w->m.run = 0;
	w->m.r_right = 0;
	w->m.r_left = 0;
	w->m.up = 0;
	w->m.down = 0;
	w->m.look = 0;
}

static void	init_t(t_wolf *w)
{
	w->t.font = NULL;
	w->t.txt_main = NULL;
	w->t.txt_sub = NULL;
	w->t.txt_tex_main = NULL;
	w->t.txt_tex_sub = NULL;
	w->t.txt_color.r = 255;
	w->t.txt_color.g = 255;
	w->t.txt_color.b = 255;
	w->t.txt_color.a = 255;
}

static int	init(t_wolf *w)
{
	w->win = NULL;
	w->ren = NULL;
	w->i.wall = NULL;
	w->i.wall2 = NULL;
	w->i.door = NULL;
	w->gameover = 0;
	w->key = 0;
	w->reset = 0;
	w->s.key = NULL;
	w->level = NULL;
	init_more(w);
	init_t(w);
	init_c(w);
	if (open_map(w) == -1)
		return (-1);
	if (texture_init(w) == -1)
		return (-1);
	if (sprite_init(w) == -1)
		return (-1);
	if (init_sdl(w) == -1)
		return (-1);
	SDL_ShowCursor(SDL_DISABLE);
	return (0);
}

int			main(void)
{
	t_wolf		w;

	if (init(&w) == -1)
		return (clean_up(-1, &w));
	while (w.gameover == 0)
	{
		while (SDL_PollEvent(&w.e))
		{
			if ((w.e.type == SDL_KEYDOWN && w.e.KEY == SDLK_ESCAPE) ||
					w.e.type == SDL_QUIT)
				w.gameover = 1;
			events(&w);
		}
		SDL_RenderClear(w.ren);
		play_sound(&w);
		mouse_ev(&w);
		rotate(&w);
		move(&w);
		raycast(&w);
		cast_sprite(&w);
		render_text(&w);
		objectives(&w);
		SDL_RenderPresent(w.ren);
	}
	return (clean_up(0, &w));
}
