/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sprite.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/10 15:00:24 by mborde            #+#    #+#             */
/*   Updated: 2015/06/12 20:01:57 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static Uint32	getpixel_sprite(int x, int y, t_wolf *w)
{
	Uint32	*pixels;

	pixels = NULL;
	pixels = (Uint32 *)w->s.key->pixels;
	return (pixels[(TEX_H * y) + x]);
}

static void		draw_sprite(int x, double fog, t_wolf *w)
{
	int		tx;
	int		ty;
	int		d;
	Uint32	color;
	Uint32	rgb[3];

	tx = (int)(256 * (x - (-w->s.sw / 2 + w->s.screenx)) * 64 / w->s.sw) / 256;
	if (w->s.transy > 0 && x > 0 && x < WIN_X && w->s.transy < w->s.zbuf[x])
	{
		w->s.y = w->s.starty;
		while (w->s.y < w->s.stopy)
		{
			d = (w->s.y - (MS - w->m.look)) * 256 - WIN_Y * 128 + w->s.sh * 128;
			ty = ((d * TEX_H) / w->s.sh) / 256;
			color = getpixel_sprite(tx, ty, w);
			make_color(color, rgb, 1, w);
			SDL_SetRenderDrawColor(w->ren, rgb[0] / fog, rgb[1] / fog,
					rgb[2] / fog, 255);
			if (rgb[0] != 0 && rgb[1] != 0 && rgb[2] != 0)
				SDL_RenderDrawPoint(w->ren, x, w->s.y);
			w->s.y++;
		}
	}
}

static void		sprite_wh(t_wolf *w)
{
	w->s.sh = abs((int)(WIN_Y / (w->s.transy)) / 2);
	w->s.starty = -w->s.sh / 2 + WIN_Y / 2 + (MS - w->m.look);
	if (w->s.starty < 0)
		w->s.starty = 0;
	w->s.stopy = w->s.sh / 2 + WIN_Y / 2 + (MS - w->m.look);
	if (w->s.stopy >= WIN_Y)
		w->s.stopy = WIN_Y - 1;
	w->s.sw = abs((int)(WIN_Y / (w->s.transy)) / 2);
	w->s.startx = -w->s.sw / 2 + w->s.screenx;
	if (w->s.startx < 0)
		w->s.startx = 0;
	w->s.stopx = w->s.sh / 2 + w->s.screenx;
	if (w->s.stopx >= WIN_X)
		w->s.stopx = WIN_X - 1;
}

void			cast_sprite(t_wolf *w)
{
	int		x;
	double	fog;

	if (w->key == 0)
	{
		w->s.sx = ((double)w->s.kx + 0.5) - w->c.posx;
		w->s.sy = ((double)w->s.ky + 0.5) - w->c.posy;
		w->s.inv = 1.0 / (w->c.planx * w->c.diry - w->c.dirx * w->c.plany);
		w->s.transx = w->s.inv * (w->c.diry * w->s.sx - w->c.dirx * w->s.sy);
		w->s.transy = w->s.inv * (-w->c.plany * w->s.sx + w->c.planx * w->s.sy);
		w->s.screenx = (int)((WIN_X / 2) * (1 + w->s.transx / w->s.transy));
		sprite_wh(w);
		x = w->s.startx;
		while (x < w->s.stopx)
		{
			fog = w->s.transy / 0.9 * 0.75;
			if (fog > 254)
				fog = 254;
			else if (fog < 1)
				fog = 1;
			draw_sprite(x, fog, w);
			x++;
		}
	}
}

int				sprite_init(t_wolf *w)
{
	SDL_Surface	*tmp;

	tmp = NULL;
	if ((tmp = SDL_LoadBMP("data/key.bmp")) == NULL)
		return (put_error("Error: a texture failed to load."));
	if (tmp->h != TEX_H || tmp->w != TEX_W)
	{
		SDL_FreeSurface(tmp);
		return (put_error("Error: wrong texture format"));
	}
	w->s.key = SDL_ConvertSurfaceFormat(tmp, SDL_PIXELFORMAT_ARGB8888, 0);
	SDL_FreeSurface(tmp);
	return (0);
}
