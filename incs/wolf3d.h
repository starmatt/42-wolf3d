/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf3d.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/29 10:13:41 by mborde            #+#    #+#             */
/*   Updated: 2015/06/12 20:02:07 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF3D_H
# define WOLF3D_H
# include "libft.h"
# include "SDL.h"
# include "SDL_mixer.h"
# include "SDL_ttf.h"
# include <fcntl.h>
# include <unistd.h>
# include <stdlib.h>
# include <math.h>
# include <time.h>
# define WIN_X 800
# define WIN_Y 600
# define MAP_W 24
# define MAP_H 24
# define TEX_W 64
# define TEX_H 64
# define MS 140
# define KEY key.keysym.sym

typedef struct	s_mov
{
	int		right;
	int		left;
	int		forw;
	int		back;
	int		run;
	int		r_left;
	int		r_right;
	int		up;
	int		down;
	int		look;
	double	movs;
	double	rota;
}				t_mov;

typedef struct	s_ray
{
	double	camx;
	double	ray_x;
	double	ray_y;
	double	ray_dx;
	double	ray_dy;
	int		mapx;
	int		mapy;
	double	side_x;
	double	side_y;
	double	delta_x;
	double	delta_y;
	double	wall_dist;
	int		stepx;
	int		stepy;
	int		side;
	int		line_h;
	int		y;
	double	wallx;
	int		top;
	double	fog;
}				t_ray;

typedef struct	s_spr
{
	int			kx;
	int			ky;
	double		zbuf[WIN_X];
	double		dist;
	double		sx;
	double		sy;
	double		inv;
	double		transx;
	double		transy;
	int			screenx;
	int			sh;
	int			sw;
	int			starty;
	int			stopy;
	int			startx;
	int			stopx;
	int			y;
	SDL_Surface	*key;
}				t_spr;

typedef struct	s_cmr
{
	double	posx;
	double	posy;
	double	dirx;
	double	diry;
	double	planx;
	double	plany;
}				t_cmr;

typedef struct	s_txt
{
	SDL_Surface		*txt_main;
	SDL_Surface		*txt_sub;
	TTF_Font		*font;
	SDL_Texture		*txt_tex_main;
	SDL_Texture		*txt_tex_sub;
	SDL_Rect		txt_rect_main;
	SDL_Rect		txt_rect_sub;
	SDL_Rect		txt_through;
	SDL_Color		txt_color;
}				t_txt;

typedef struct	s_img
{
	SDL_Surface	*wall;
	SDL_Surface	*door;
	SDL_Surface	*wall2;
}				t_img;

typedef struct	s_wolf
{
	SDL_Window		*win;
	SDL_Renderer	*ren;
	int				fd;
	char			*level;
	int				map[MAP_W][MAP_H];
	SDL_Event		e;
	Mix_Music		*mus;
	Mix_Chunk		*footstep;
	Mix_Chunk		*footstep2;
	Mix_Chunk		*doorwav;
	Mix_Chunk		*keywav;
	t_cmr			c;
	t_ray			r;
	t_mov			m;
	t_txt			t;
	t_img			i;
	t_spr			s;
	int				key;
	int				gameover;
	int				reset;
	char			*lvln;
}				t_wolf;

void			init_more(t_wolf *w);
int				open_map(t_wolf *w);
int				get_linenb(int level);
int				put_error(char *s);
int				clean_up(int ret, t_wolf *w);
void			events(t_wolf *w);
void			raycast(t_wolf *w);
void			move(t_wolf *w);
void			mouse_ev(t_wolf *w);
void			rotate(t_wolf *w);
void			draw(int x, t_wolf *w);
int				sound_init(t_wolf *w);
void			play_sound(t_wolf *w);
int				text_init(t_wolf *w);
void			render_text(t_wolf *w);
int				texture_init(t_wolf *w);
void			make_color(Uint32 color, Uint32 *rgb, int k, t_wolf *w);
Uint32			getpixel(int x, int y, t_wolf *w);
void			objectives(t_wolf *w);
int				sprite_init(t_wolf *w);
void			cast_sprite(t_wolf *w);
void			key_pos(t_wolf *w);
void			reset(t_wolf *w);
void			init_c(t_wolf *w);

#endif
