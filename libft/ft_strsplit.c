/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/09 16:05:12 by mborde            #+#    #+#             */
/*   Updated: 2014/12/16 17:43:30 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static char		**create_tab(char const *s, char c)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (s[i])
	{
		if (s[i] != c)
		{
			j++;
			while (s[i] != c && s[i + 1] != '\0')
				i++;
		}
		i++;
	}
	return ((char **)malloc(sizeof(char *) * j + 1));
}

static char		*ret_str(char *t, char c)
{
	int	i;

	i = 0;
	while (t[i])
	{
		if (t[i] == c)
			t[i] = '\0';
		i++;
	}
	return (t);
}

static void		be_null(int i[3])
{
	i[0] = 0;
	i[1] = 0;
}

char			**ft_strsplit(char const *s, char c)
{
	char	**split;
	char	*t;
	int		i[3];

	if (s == NULL)
		return (NULL);
	t = ft_strdup(s);
	i[2] = ft_strlen(s);
	if (!(split = create_tab(s, c)))
		return (NULL);
	t = ret_str(t, c);
	be_null(i);
	while (i[0] < i[2])
	{
		if (!t[i[0]])
			i[0]++;
		else
		{
			split[i[1]] = ft_strdup(&t[i[0]]);
			i[1]++;
			i[0] += ft_strlen(&t[i[0]]);
		}
	}
	split[i[1]] = NULL;
	return (split);
}
