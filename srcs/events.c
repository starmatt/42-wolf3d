/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   events.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/28 15:17:35 by mborde            #+#    #+#             */
/*   Updated: 2015/06/12 15:49:11 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void		mouse_ev(t_wolf *w)
{
	int	x;
	int	y;

	SDL_SetRelativeMouseMode(1);
	SDL_GetRelativeMouseState(&x, &y);
	if (x > 0)
		w->m.r_right = 1;
	else
		w->m.r_right = 0;
	if (x < 0)
		w->m.r_left = 1;
	else
		w->m.r_left = 0;
	if (y > 0)
		w->m.up = 1;
	else
		w->m.up = 0;
	if (y < 0)
		w->m.down = 1;
	else
		w->m.down = 0;
}

static void	key_up(t_wolf *w)
{
	if (w->e.KEY == SDLK_w)
		w->m.forw = 0;
	if (w->e.KEY == SDLK_a)
		w->m.left = 0;
	if (w->e.KEY == SDLK_s)
		w->m.back = 0;
	if (w->e.KEY == SDLK_d)
		w->m.right = 0;
	if (w->e.KEY == SDLK_LSHIFT)
		w->m.run = 0;
}

static void	key_down(t_wolf *w)
{
	if (w->e.KEY == SDLK_w)
		w->m.forw = 1;
	if (w->e.KEY == SDLK_a)
		w->m.left = 1;
	if (w->e.KEY == SDLK_s)
		w->m.back = 1;
	if (w->e.KEY == SDLK_d)
		w->m.right = 1;
	if (w->e.KEY == SDLK_LSHIFT)
		w->m.run = 1;
}

void		events(t_wolf *w)
{
	if (w->e.type == SDL_KEYDOWN)
		key_down(w);
	if (w->e.type == SDL_KEYUP)
		key_up(w);
}
