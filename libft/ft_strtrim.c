/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/08 16:12:22 by mborde            #+#    #+#             */
/*   Updated: 2015/01/03 13:53:26 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static int	check_blank(int c)
{
	if (c == ' ' || c == '\n' || c == '\t')
		return (1);
	else
		return (0);
}

char		*ft_strtrim(const char *s)
{
	char	*cpy;
	int		i[2];
	int		l;

	i[0] = 0;
	if (s == NULL)
		return (NULL);
	l = ft_strlen(s);
	cpy = ft_strnew(l + 1);
	ft_memcpy(cpy, s, l);
	while (cpy[i[0]] && check_blank(cpy[i[0]]))
		i[0]++;
	i[1] = l - 1;
	while (i[1] > 0)
	{
		if (check_blank(cpy[i[1]]))
			i[1]--;
		else
		{
			cpy[i[1] + 1] = '\0';
			break ;
		}
	}
	return (ft_strdup(&cpy[i[0]]));
}
